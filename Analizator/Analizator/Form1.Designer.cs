﻿namespace Analizator
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ImportMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuSettingsFilter = new System.Windows.Forms.ToolStripMenuItem();
            this.StatisticMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.TableData = new System.Windows.Forms.DataGridView();
            this.bindSourceTableData = new System.Windows.Forms.BindingSource(this.components);
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TableData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindSourceTableData)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.MenuSettingsFilter,
            this.StatisticMenu});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1158, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ImportMenu});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // ImportMenu
            // 
            this.ImportMenu.Name = "ImportMenu";
            this.ImportMenu.Size = new System.Drawing.Size(118, 22);
            this.ImportMenu.Text = "Импорт";
            this.ImportMenu.Click += new System.EventHandler(this.ImportMenu_Click);
            // 
            // MenuSettingsFilter
            // 
            this.MenuSettingsFilter.Name = "MenuSettingsFilter";
            this.MenuSettingsFilter.Size = new System.Drawing.Size(129, 20);
            this.MenuSettingsFilter.Text = "Настройки фильтра";
            this.MenuSettingsFilter.Click += new System.EventHandler(this.MenuSettingsFilter_Click);
            // 
            // StatisticMenu
            // 
            this.StatisticMenu.Name = "StatisticMenu";
            this.StatisticMenu.Size = new System.Drawing.Size(80, 20);
            this.StatisticMenu.Text = "Статистика";
            this.StatisticMenu.Click += new System.EventHandler(this.StatisticMenu_Click);
            // 
            // TableData
            // 
            this.TableData.AllowUserToAddRows = false;
            this.TableData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TableData.Location = new System.Drawing.Point(26, 42);
            this.TableData.Name = "TableData";
            this.TableData.RowHeadersVisible = false;
            this.TableData.Size = new System.Drawing.Size(1088, 487);
            this.TableData.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1158, 569);
            this.Controls.Add(this.TableData);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TableData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindSourceTableData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ImportMenu;
        private System.Windows.Forms.ToolStripMenuItem MenuSettingsFilter;
        private System.Windows.Forms.ToolStripMenuItem StatisticMenu;
        private System.Windows.Forms.DataGridView TableData;
        private System.Windows.Forms.BindingSource bindSourceTableData;
    }
}

