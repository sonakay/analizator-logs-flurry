﻿namespace Analizator
{
    partial class Statistic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.Cancel = new System.Windows.Forms.Button();
            this.BoxSelectParametr = new System.Windows.Forms.GroupBox();
            this.chartParametr = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.RunStatisticAnalyze = new System.Windows.Forms.Button();
            this.IsEnableLabelData = new System.Windows.Forms.CheckBox();
            this.TableDataForChart = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.chartParametr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TableDataForChart)).BeginInit();
            this.SuspendLayout();
            // 
            // Cancel
            // 
            this.Cancel.Location = new System.Drawing.Point(1235, 585);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(132, 30);
            this.Cancel.TabIndex = 0;
            this.Cancel.Text = "Отмена";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // BoxSelectParametr
            // 
            this.BoxSelectParametr.Location = new System.Drawing.Point(29, 12);
            this.BoxSelectParametr.Name = "BoxSelectParametr";
            this.BoxSelectParametr.Size = new System.Drawing.Size(962, 45);
            this.BoxSelectParametr.TabIndex = 1;
            this.BoxSelectParametr.TabStop = false;
            this.BoxSelectParametr.Text = "Выберите параметр:";
            // 
            // chartParametr
            // 
            this.chartParametr.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            chartArea1.Name = "ChartArea1";
            this.chartParametr.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartParametr.Legends.Add(legend1);
            this.chartParametr.Location = new System.Drawing.Point(302, 63);
            this.chartParametr.Name = "chartParametr";
            this.chartParametr.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series1.CustomProperties = "MinimumRelativePieSize=50, PieLabelStyle=Outside";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            series1.SmartLabelStyle.CalloutStyle = System.Windows.Forms.DataVisualization.Charting.LabelCalloutStyle.Box;
            this.chartParametr.Series.Add(series1);
            this.chartParametr.Size = new System.Drawing.Size(1032, 516);
            this.chartParametr.TabIndex = 2;
            this.chartParametr.Text = "Diagramm";
            // 
            // RunStatisticAnalyze
            // 
            this.RunStatisticAnalyze.Enabled = false;
            this.RunStatisticAnalyze.Location = new System.Drawing.Point(46, 63);
            this.RunStatisticAnalyze.Name = "RunStatisticAnalyze";
            this.RunStatisticAnalyze.Size = new System.Drawing.Size(121, 23);
            this.RunStatisticAnalyze.TabIndex = 3;
            this.RunStatisticAnalyze.Text = "Построить";
            this.RunStatisticAnalyze.UseVisualStyleBackColor = true;
            this.RunStatisticAnalyze.Click += new System.EventHandler(this.RunStatisticAnalyze_Click);
            // 
            // IsEnableLabelData
            // 
            this.IsEnableLabelData.AutoSize = true;
            this.IsEnableLabelData.Location = new System.Drawing.Point(475, 598);
            this.IsEnableLabelData.Name = "IsEnableLabelData";
            this.IsEnableLabelData.Size = new System.Drawing.Size(177, 17);
            this.IsEnableLabelData.TabIndex = 4;
            this.IsEnableLabelData.Text = "Вкл. / Выкл.  подписи данных";
            this.IsEnableLabelData.UseVisualStyleBackColor = true;
            this.IsEnableLabelData.CheckedChanged += new System.EventHandler(this.IsEnableLabelData_CheckedChanged);
            // 
            // TableDataForChart
            // 
            this.TableDataForChart.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TableDataForChart.Location = new System.Drawing.Point(12, 107);
            this.TableDataForChart.Name = "TableDataForChart";
            this.TableDataForChart.Size = new System.Drawing.Size(261, 508);
            this.TableDataForChart.TabIndex = 5;
            // 
            // Statistic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1379, 627);
            this.Controls.Add(this.TableDataForChart);
            this.Controls.Add(this.IsEnableLabelData);
            this.Controls.Add(this.RunStatisticAnalyze);
            this.Controls.Add(this.chartParametr);
            this.Controls.Add(this.BoxSelectParametr);
            this.Controls.Add(this.Cancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Statistic";
            this.Text = "Statistic";
            ((System.ComponentModel.ISupportInitialize)(this.chartParametr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TableDataForChart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.GroupBox BoxSelectParametr;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartParametr;
        private System.Windows.Forms.Button RunStatisticAnalyze;
        private System.Windows.Forms.CheckBox IsEnableLabelData;
        private System.Windows.Forms.DataGridView TableDataForChart;
    }
}