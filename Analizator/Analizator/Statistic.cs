﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Analizator
{
    public partial class Statistic : Form
    {
        /// <summary>
        /// Имя текущего выбранного параметра
        /// </summary>
        private string CurrentSelect = string.Empty;
        /// <summary>
        /// Таблица со всеми данными
        /// </summary>
        DataGridView _table;
        /// <summary>
        /// Словарь, содержащий данные для диаграммы: ключ - значение параметра, значение - количество встреч параметра с этим значением
        /// </summary>
        Dictionary<string, int> dataForCharting;
        /// <summary>
        /// Общее количество значений параметров
        /// </summary>
        private int _generalCountParametrs =0 ;
        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public Statistic()
        {
            InitializeComponent();
            _initialize();
        }
        /// <summary>
        /// Конструктор, принимающий таблицу со всеми данными
        /// </summary>
        /// <param name="table"></param>
        public Statistic(DataGridView table)
        {
            InitializeComponent();
            _initialize();
            _table = table;
            
        }       
        /// <summary>
        /// Функция, инициализирующая RadioButtons, отвечающих за выбор иследуемого параметра 
        /// </summary>
        private void _initialize()
        {
            //положение  по координате x
            int pozX = 10;
            //положение по координате y
            int pozY = 20;
            //ширина предыдущего RadioButton
            int width = 0;
            //количество, созданных RadioButton
            int count = 0;
            //создаваемый RadioButton
            RadioButton radioButton;
            foreach( var item in Content.Instance.Parametrs)
            {
                radioButton = new RadioButton();
                radioButton.Name = item;
                radioButton.Text = item;
                //pozX+ indent* count - каждый следующий RadioButton будет находится от другого на растоянии posX
                radioButton.Location = new Point(pozX+ width* count, pozY);
                radioButton.CheckedChanged += SelectParametr_CheckedChanged;
                width = radioButton.Width;
                count++;
                //добавление в родительский элемент
                BoxSelectParametr.Controls.Add(radioButton);               
            }
        }
        //Событие выбора одного из RadioButton
        private void SelectParametr_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton selectRadioButton = (RadioButton)sender;
            CurrentSelect = selectRadioButton.Text;
            RunStatisticAnalyze.Enabled = true;            
        }
        /// <summary>
        /// Функция, рисующая диаграмму
        /// </summary>
        private void DrawDiagramma()
        {
            chartParametr.Titles.Add(CurrentSelect);
            foreach(var item in dataForCharting)
            {
                DataPoint point =  new DataPoint();
                point.LegendText = item.Key;
                point.SetValueY(item.Value);             
                chartParametr.Series[0].Points.Add(point);
            }        
        }
        /// <summary>
        /// Функция заполнения словаря
        /// </summary>
        private void SetDictionary()
        {
            _generalCountParametrs = 0;
            //номер стоблца, содержащий выбранный параметр
            int indexSelectParametr =0 ;            
            //определение номера стоблца, содержащего номер выбранного словаря
            for(int i =0; i < _table.ColumnCount; i++)
            {
                if(_table.Columns[i].HeaderText == CurrentSelect)
                {
                    indexSelectParametr = i;
                }
            }
            //Заполнение словаря
            for(int i=0; i < _table.RowCount; i++)
            {
               
                //Если строка не была скрыта при фильтре

                    if(dataForCharting.ContainsKey(_table.Rows[i].Cells[indexSelectParametr].Value.ToString()))
                    {
                        dataForCharting[_table.Rows[i].Cells[indexSelectParametr].Value.ToString()]++;
                    }
                    else
                    {
                        dataForCharting.Add(_table.Rows[i].Cells[indexSelectParametr].Value.ToString(), 1);
                    }
                    _generalCountParametrs++;
            }
        }
        //Событие нажатие кнопки Отмена
        private void Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //Событие, реагирующее на запуск статистического параметра
        private void RunStatisticAnalyze_Click(object sender, EventArgs e)
        {
            chartParametr.Titles.Clear();
            chartParametr.Series[0].Points.Clear();
            dataForCharting = new Dictionary<string, int>();
            SetDictionary();
            DrawDiagramma();
            _setTable();
        }
        //Событие, реагирующее на выбор наличия подписей к данным на диаграмме
        private void IsEnableLabelData_CheckedChanged(object sender, EventArgs e)
        {
            chartParametr.Series["Series1"].IsValueShownAsLabel = IsEnableLabelData.Checked;
        }
        /// <summary>
        /// Функция заполнения таблицы
        /// </summary>
        private void _setTable()
        {
            TableDataForChart.ColumnCount = 3;
            TableDataForChart.RowCount = dataForCharting.Count;
            TableDataForChart.Columns[0].HeaderText ="Параметр"+CurrentSelect;
            TableDataForChart.Columns[1].HeaderText = "Количество значений";
            TableDataForChart.Columns[2].HeaderText = "Процент от общего числа, %";
            //Номер строки
            int numberRow = 0;
                foreach(var item in dataForCharting)
                {
                    TableDataForChart.Rows[numberRow].Cells[0].Value = item.Key;
                    TableDataForChart.Rows[numberRow].Cells[1].Value = item.Value;
                   TableDataForChart.Rows[numberRow].Cells[2].Value = Math.Round(((decimal)(item.Value) / (decimal)(_generalCountParametrs))*100,3);
                    numberRow++;
                }                
        }
    }
}
