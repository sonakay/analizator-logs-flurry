﻿namespace Analizator
{
    partial class SettingsFilter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Cancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.SelectValueTimestamp = new System.Windows.Forms.ComboBox();
            this.SelectValueSessionIndex = new System.Windows.Forms.ComboBox();
            this.SelectValueEvent = new System.Windows.Forms.ComboBox();
            this.SelectValueDescription = new System.Windows.Forms.ComboBox();
            this.SelectValueVersion = new System.Windows.Forms.ComboBox();
            this.SelectValuePlatform = new System.Windows.Forms.ComboBox();
            this.SelectValueDevice = new System.Windows.Forms.ComboBox();
            this.SelectValueUserID = new System.Windows.Forms.ComboBox();
            this.ActivateFilter = new System.Windows.Forms.Button();
            this.ContainerForParametrsFieldParams = new System.Windows.Forms.GroupBox();
            this.OffFilter = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Cancel
            // 
            this.Cancel.Location = new System.Drawing.Point(784, 377);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(100, 38);
            this.Cancel.TabIndex = 0;
            this.Cancel.Text = "Отмена";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(34, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Timestamp:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(34, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Session Index:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(34, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Event";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(34, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Description:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(34, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "Version:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(34, 204);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "Platform:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(34, 236);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 17);
            this.label7.TabIndex = 7;
            this.label7.Text = "Device";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(36, 266);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 17);
            this.label8.TabIndex = 8;
            this.label8.Text = "User ID:";
            // 
            // SelectValueTimestamp
            // 
            this.SelectValueTimestamp.FormattingEnabled = true;
            this.SelectValueTimestamp.Items.AddRange(new object[] {
            "Любое значение"});
            this.SelectValueTimestamp.Location = new System.Drawing.Point(159, 39);
            this.SelectValueTimestamp.Name = "SelectValueTimestamp";
            this.SelectValueTimestamp.Size = new System.Drawing.Size(375, 21);
            this.SelectValueTimestamp.TabIndex = 9;
            // 
            // SelectValueSessionIndex
            // 
            this.SelectValueSessionIndex.FormattingEnabled = true;
            this.SelectValueSessionIndex.Items.AddRange(new object[] {
            "Любое значение"});
            this.SelectValueSessionIndex.Location = new System.Drawing.Point(159, 67);
            this.SelectValueSessionIndex.Name = "SelectValueSessionIndex";
            this.SelectValueSessionIndex.Size = new System.Drawing.Size(375, 21);
            this.SelectValueSessionIndex.TabIndex = 10;
            // 
            // SelectValueEvent
            // 
            this.SelectValueEvent.FormattingEnabled = true;
            this.SelectValueEvent.Items.AddRange(new object[] {
            "Любое значение"});
            this.SelectValueEvent.Location = new System.Drawing.Point(159, 99);
            this.SelectValueEvent.Name = "SelectValueEvent";
            this.SelectValueEvent.Size = new System.Drawing.Size(375, 21);
            this.SelectValueEvent.TabIndex = 11;
            // 
            // SelectValueDescription
            // 
            this.SelectValueDescription.FormattingEnabled = true;
            this.SelectValueDescription.Items.AddRange(new object[] {
            "Любое значение"});
            this.SelectValueDescription.Location = new System.Drawing.Point(159, 130);
            this.SelectValueDescription.Name = "SelectValueDescription";
            this.SelectValueDescription.Size = new System.Drawing.Size(375, 21);
            this.SelectValueDescription.TabIndex = 12;
            // 
            // SelectValueVersion
            // 
            this.SelectValueVersion.FormattingEnabled = true;
            this.SelectValueVersion.Items.AddRange(new object[] {
            "Любое значение"});
            this.SelectValueVersion.Location = new System.Drawing.Point(159, 164);
            this.SelectValueVersion.Name = "SelectValueVersion";
            this.SelectValueVersion.Size = new System.Drawing.Size(375, 21);
            this.SelectValueVersion.TabIndex = 13;
            // 
            // SelectValuePlatform
            // 
            this.SelectValuePlatform.FormattingEnabled = true;
            this.SelectValuePlatform.Items.AddRange(new object[] {
            "Любое значение"});
            this.SelectValuePlatform.Location = new System.Drawing.Point(159, 204);
            this.SelectValuePlatform.Name = "SelectValuePlatform";
            this.SelectValuePlatform.Size = new System.Drawing.Size(375, 21);
            this.SelectValuePlatform.TabIndex = 14;
            // 
            // SelectValueDevice
            // 
            this.SelectValueDevice.FormattingEnabled = true;
            this.SelectValueDevice.Items.AddRange(new object[] {
            "Любое значение"});
            this.SelectValueDevice.Location = new System.Drawing.Point(159, 236);
            this.SelectValueDevice.Name = "SelectValueDevice";
            this.SelectValueDevice.Size = new System.Drawing.Size(375, 21);
            this.SelectValueDevice.TabIndex = 15;
            // 
            // SelectValueUserID
            // 
            this.SelectValueUserID.FormattingEnabled = true;
            this.SelectValueUserID.Items.AddRange(new object[] {
            "Любое значение"});
            this.SelectValueUserID.Location = new System.Drawing.Point(159, 266);
            this.SelectValueUserID.Name = "SelectValueUserID";
            this.SelectValueUserID.Size = new System.Drawing.Size(375, 21);
            this.SelectValueUserID.TabIndex = 16;
            // 
            // ActivateFilter
            // 
            this.ActivateFilter.Location = new System.Drawing.Point(48, 317);
            this.ActivateFilter.Name = "ActivateFilter";
            this.ActivateFilter.Size = new System.Drawing.Size(161, 32);
            this.ActivateFilter.TabIndex = 17;
            this.ActivateFilter.Text = "Активировать фильтр";
            this.ActivateFilter.UseVisualStyleBackColor = true;
            this.ActivateFilter.Click += new System.EventHandler(this.ActivateFilter_Click);
            // 
            // ContainerForParametrsFieldParams
            // 
            this.ContainerForParametrsFieldParams.Location = new System.Drawing.Point(579, 34);
            this.ContainerForParametrsFieldParams.Name = "ContainerForParametrsFieldParams";
            this.ContainerForParametrsFieldParams.Size = new System.Drawing.Size(321, 337);
            this.ContainerForParametrsFieldParams.TabIndex = 20;
            this.ContainerForParametrsFieldParams.TabStop = false;
            this.ContainerForParametrsFieldParams.Text = "Параметры поля Params";
            // 
            // OffFilter
            // 
            this.OffFilter.Enabled = false;
            this.OffFilter.Location = new System.Drawing.Point(215, 317);
            this.OffFilter.Name = "OffFilter";
            this.OffFilter.Size = new System.Drawing.Size(166, 32);
            this.OffFilter.TabIndex = 21;
            this.OffFilter.Text = "Отключить фильтр";
            this.OffFilter.UseVisualStyleBackColor = true;
            this.OffFilter.Click += new System.EventHandler(this.OffFilter_Click);
            // 
            // SettingsFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(916, 424);
            this.Controls.Add(this.OffFilter);
            this.Controls.Add(this.ContainerForParametrsFieldParams);
            this.Controls.Add(this.ActivateFilter);
            this.Controls.Add(this.SelectValueUserID);
            this.Controls.Add(this.SelectValueDevice);
            this.Controls.Add(this.SelectValuePlatform);
            this.Controls.Add(this.SelectValueVersion);
            this.Controls.Add(this.SelectValueDescription);
            this.Controls.Add(this.SelectValueEvent);
            this.Controls.Add(this.SelectValueSessionIndex);
            this.Controls.Add(this.SelectValueTimestamp);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Cancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsFilter";
            this.Text = "SettingsFilter";
            this.Load += new System.EventHandler(this.SettingsFilter_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox SelectValueTimestamp;
        private System.Windows.Forms.ComboBox SelectValueSessionIndex;
        private System.Windows.Forms.ComboBox SelectValueEvent;
        private System.Windows.Forms.ComboBox SelectValueDescription;
        private System.Windows.Forms.ComboBox SelectValueVersion;
        private System.Windows.Forms.ComboBox SelectValuePlatform;
        private System.Windows.Forms.ComboBox SelectValueDevice;
        private System.Windows.Forms.ComboBox SelectValueUserID;
        private System.Windows.Forms.Button ActivateFilter;
        private System.Windows.Forms.GroupBox ContainerForParametrsFieldParams;
        private System.Windows.Forms.Button OffFilter;
    }
}