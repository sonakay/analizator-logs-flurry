﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AnalizatorLogsEventFlurry;

namespace Analizator
{
    public partial class Import : Form
    {
        /// <summary>
        /// Таблица для начальных данных
        /// </summary>
        private DataGridView _table;
        /// <summary>
        /// Binding Source, привязанный к таблице с загружеными из файла данными
        /// </summary>
        private BindingSource _bind;
        /// <summary>
        /// Объект для загрузки данных
        /// </summary>
        LoadFile ObjLoad = new LoadFile();
        /// <summary>
        /// Конструтор по умолчанию
        /// </summary>
        public Import()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Конструктор для заполнения данных таблицы с помощью Binding Source
        /// </summary>
        /// <param name="table"></param>
        /// <param name="bind">Binding Source, привязанный к таблице с загружеными из файла данными</param>
        public Import(DataGridView table, BindingSource bind)
        {
            InitializeComponent();
            _table = table;
            _bind = bind;
        }
        //Выбрали/Отменили выбор всех полей
        private void SelectAll_CheckedChanged(object sender, EventArgs e)
        {
            SelectTimestamp.Checked = SelectAll.Checked;
            SelectSessionIndex.Checked = SelectAll.Checked;
            SelectEvent.Checked = SelectAll.Checked;
            SelectDescription.Checked = SelectAll.Checked;
            SelectVersion.Checked = SelectAll.Checked;
            SelectPlatform.Checked = SelectAll.Checked;
            SelectDevice.Checked = SelectAll.Checked;
            SelectUser.Checked = SelectAll.Checked;
            LoadFile.SetAllIsNeed(SelectAll.Checked);
        }
        //Выбрали/Отменили выбор поля Timestamp
        private void SelectTimestamp_CheckedChanged(object sender, EventArgs e)
        {
            LoadFile.IsNeedTimestamp = SelectTimestamp.Checked;
        }
        //Выбрали/Отменили выбор поля Session Index
        private void SelectSessionIndex_CheckedChanged(object sender, EventArgs e)
        {
            LoadFile.IsNeedSessionIndex = SelectSessionIndex.Checked;
        }
        //Выбрали/Отменили выбор поля Event
        private void SelectEvent_CheckedChanged(object sender, EventArgs e)
        {
            LoadFile.IsNeedEvent = SelectEvent.Checked;     
        }
        //Выбрали/Отменили выбор поля Description
        private void SelectDescription_CheckedChanged(object sender, EventArgs e)
        {
            LoadFile.IsNeedDescription = SelectDescription.Checked;
        }
        //Выбрали/Отменили выбор поля Version
        private void SelectVersion_CheckedChanged(object sender, EventArgs e)
        {
            LoadFile.IsNeedVersion = SelectVersion.Checked;
        }
        //Выбрали/Отменили выбор поля Platform
        private void SelectPlatform_CheckedChanged(object sender, EventArgs e)
        {
            LoadFile.IsNeedPlatform = SelectPlatform.Checked;
        }
        //Выбрали/Отменили выбор поля Device
        private void SelectDevice_CheckedChanged(object sender, EventArgs e)
        {
            LoadFile.IsNeedDevice = SelectDevice.Checked;
        }
        //Выбрали/Отменили выбор поля User ID
        private void SelectUser_CheckedChanged(object sender, EventArgs e)
        {
            LoadFile.IsNeedUserID = SelectUser.Checked;
        }
        //Окрытие файла
        private void OpenFile_Click(object sender, EventArgs e)
        {
            ObjLoad.OpenFile();
            if (LoadFile.isSuccessfulOpen)
            {
                 try
                {
                    _setTable();
                    this.Close();

                }               
                catch (Exception ed)
                {
                    LoadFile.isSuccessfulOpen = false;
                    MessageBox.Show(ed.Message + "\nДополнительно: Некоректный формат файла");
                }
            }
        }
        //Закрытие окна посредством кнопки отмена
        private void Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// Заполнение таблицы
        /// </summary>
        private void _setTable()
        {
            DataTable dataTable = new DataTable();
            for (int i = 0; i < ObjLoad.CountColumn; i++)
            {
                dataTable.Columns.Add(ObjLoad.NameColumnTable[i]);
            }

            DataRow newRow;
            for (int i = 0; i < ObjLoad.CountRow; i++)
            {
                newRow = dataTable.NewRow();
                for (int j = 0; j < ObjLoad.CountColumn; j++)
                {
                    newRow[j] = (ObjLoad.Result[i, j]);
                }                
                dataTable.Rows.Add(newRow);

            }
            _bind.DataSource = dataTable;
            _table.DataSource = _bind;

            var check = new bool[]
              {
                 LoadFile.IsNeedTimestamp ,
                 LoadFile.IsNeedSessionIndex,
                 LoadFile.IsNeedEvent,
                 LoadFile.IsNeedDescription,
                 LoadFile.IsNeedVersion,
                 LoadFile.IsNeedPlatform,
                 LoadFile.IsNeedDevice,
                 LoadFile.IsNeedUserID
             };
            for (int i = 0; i < check.Length; i++)
            {
                _table.Columns[i].Visible = check[i];
            }
        }
    }
}
