﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analizator
{
    class Content
    {
        /// <summary>
        /// Список всех колонок таблицы
        /// </summary>
        public List<string> NameColumn;
        /// <summary>
        /// Список, доступных значений поля Timestamp
        /// </summary>
        public List<string> ValueTimestamp;
        /// <summary>
        /// Список, доступных знаечний поля Session Index
        /// </summary>
        public List<string> ValueSessionIndex;
        /// <summary>
        /// Список, доступных значений поля Event
        /// </summary>
        public List<string> ValueEvent;
        /// <summary>
        /// Список, доступных значений поля Description 
        /// </summary>
        public List<string> ValueDescription;
        /// <summary>
        /// Список, доступных значений поля Device
        /// </summary>
        public List<string> ValueDevice;
        /// <summary>
        /// Список, доступных значений поля Version
        /// </summary>
        public List<string> ValueVersion;
        /// <summary>
        /// Список, доступных значений поля Platform
        /// </summary>
        public List<string> ValuePlatform;
        /// <summary>
        /// Список, доступных значений поля User ID
        /// </summary>
        public List<string> ValueUserID;
        /// <summary>
        /// Список, названий параметров
        /// </summary>
        public List<string> Parametrs;
        /// <summary>
        /// Словарь уникальных параметров поля Params. Ключ - имя параметра, значение - список уникальных значений параметра
        /// </summary>
        public Dictionary<string, List<string>> ValueParametr;
        /// <summary>
        /// Поле объекта этого класса
        /// </summary>
        private static Content _instance;
        /// <summary>
        /// Активирован ли фильтр?
        /// </summary>
        public bool isFilterActive = false;
        /// <summary>
        /// Свойство, возвращающее объект данного класса
        /// </summary>
        public static Content Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Content();
                }
                return _instance;
            }
        }
        /// <summary>
        /// Конструктор данного класса
        /// </summary>
        private Content()
        {
            ValueTimestamp = new List<string>();
            ValueTimestamp.Add(" ");

            ValueSessionIndex = new List<string>();
            ValueSessionIndex.Add(" ");

            ValueEvent = new List<string>();
            ValueEvent.Add(" ");

            ValueDescription = new List<string>();
            ValueDescription.Add(" ");

            ValueVersion = new List<string>();
            ValueVersion.Add(" ");

            ValuePlatform = new List<string>();
            ValuePlatform.Add(" ");

            ValueUserID = new List<string>();
            ValueUserID.Add(" ");

            ValueDevice = new List<string>();
            ValueDevice.Add(" ");

            ValueParametr = new Dictionary<string, List<string>>();

            Parametrs = new List<string>();
        }
        /// <summary>
        /// Функция заполнения списков
        /// </summary>
        /// <param name="row">Строка, которую необходимо добавить в список</param>
        /// <param name="list">Список, в который необходимо добавить строку</param>
        public void AddList(string row, List<string> list)
        {
            if (!list.Contains(row))
            {
                list.Add(row);
            }
        }
        /// <summary>
        /// Функция заполнения словаря ValueParametr
        /// </summary>
        /// <param name="nameParametr">Имя параметра</param>
        /// <param name="valueParametr">Значение параметра</param>
        public void AddDictionary(string nameParametr, string valueParametr)
        {
            if(ValueParametr.ContainsKey(nameParametr))
            {
                if (!ValueParametr[nameParametr].Contains(valueParametr))
                {
                    ValueParametr[nameParametr].Add(valueParametr);
                }          
            }
            else
            {
                ValueParametr.Add(nameParametr, new List<string>() {" ", valueParametr });
            }
        }

        public void Clear()
        {
            ValueParametr.Clear();
        }
    }
}
