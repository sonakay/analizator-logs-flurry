﻿namespace Analizator
{
    partial class Import
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SelectUser = new System.Windows.Forms.CheckBox();
            this.SelectDevice = new System.Windows.Forms.CheckBox();
            this.SelectPlatform = new System.Windows.Forms.CheckBox();
            this.SelectVersion = new System.Windows.Forms.CheckBox();
            this.SelectDescription = new System.Windows.Forms.CheckBox();
            this.SelectEvent = new System.Windows.Forms.CheckBox();
            this.SelectSessionIndex = new System.Windows.Forms.CheckBox();
            this.SelectTimestamp = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SelectAll = new System.Windows.Forms.CheckBox();
            this.OpenFile = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SelectUser);
            this.groupBox1.Controls.Add(this.SelectDevice);
            this.groupBox1.Controls.Add(this.SelectPlatform);
            this.groupBox1.Controls.Add(this.SelectVersion);
            this.groupBox1.Controls.Add(this.SelectDescription);
            this.groupBox1.Controls.Add(this.SelectEvent);
            this.groupBox1.Controls.Add(this.SelectSessionIndex);
            this.groupBox1.Controls.Add(this.SelectTimestamp);
            this.groupBox1.Location = new System.Drawing.Point(59, 122);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(246, 217);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Список полей";
            // 
            // SelectUser
            // 
            this.SelectUser.AutoSize = true;
            this.SelectUser.Location = new System.Drawing.Point(18, 191);
            this.SelectUser.Name = "SelectUser";
            this.SelectUser.Size = new System.Drawing.Size(59, 17);
            this.SelectUser.TabIndex = 5;
            this.SelectUser.Text = "UserID";
            this.SelectUser.UseVisualStyleBackColor = true;
            this.SelectUser.CheckedChanged += new System.EventHandler(this.SelectUser_CheckedChanged);
            // 
            // SelectDevice
            // 
            this.SelectDevice.AutoSize = true;
            this.SelectDevice.Location = new System.Drawing.Point(17, 168);
            this.SelectDevice.Name = "SelectDevice";
            this.SelectDevice.Size = new System.Drawing.Size(60, 17);
            this.SelectDevice.TabIndex = 4;
            this.SelectDevice.Text = "Device";
            this.SelectDevice.UseVisualStyleBackColor = true;
            this.SelectDevice.CheckedChanged += new System.EventHandler(this.SelectDevice_CheckedChanged);
            // 
            // SelectPlatform
            // 
            this.SelectPlatform.AutoSize = true;
            this.SelectPlatform.Location = new System.Drawing.Point(18, 145);
            this.SelectPlatform.Name = "SelectPlatform";
            this.SelectPlatform.Size = new System.Drawing.Size(64, 17);
            this.SelectPlatform.TabIndex = 3;
            this.SelectPlatform.Text = "Platform";
            this.SelectPlatform.UseVisualStyleBackColor = true;
            this.SelectPlatform.CheckedChanged += new System.EventHandler(this.SelectPlatform_CheckedChanged);
            // 
            // SelectVersion
            // 
            this.SelectVersion.AutoSize = true;
            this.SelectVersion.Location = new System.Drawing.Point(18, 122);
            this.SelectVersion.Name = "SelectVersion";
            this.SelectVersion.Size = new System.Drawing.Size(61, 17);
            this.SelectVersion.TabIndex = 1;
            this.SelectVersion.Text = "Version";
            this.SelectVersion.UseVisualStyleBackColor = true;
            this.SelectVersion.CheckedChanged += new System.EventHandler(this.SelectVersion_CheckedChanged);
            // 
            // SelectDescription
            // 
            this.SelectDescription.AutoSize = true;
            this.SelectDescription.Location = new System.Drawing.Point(18, 99);
            this.SelectDescription.Name = "SelectDescription";
            this.SelectDescription.Size = new System.Drawing.Size(79, 17);
            this.SelectDescription.TabIndex = 1;
            this.SelectDescription.Text = "Description";
            this.SelectDescription.UseVisualStyleBackColor = true;
            this.SelectDescription.CheckedChanged += new System.EventHandler(this.SelectDescription_CheckedChanged);
            // 
            // SelectEvent
            // 
            this.SelectEvent.AutoSize = true;
            this.SelectEvent.Location = new System.Drawing.Point(18, 76);
            this.SelectEvent.Name = "SelectEvent";
            this.SelectEvent.Size = new System.Drawing.Size(54, 17);
            this.SelectEvent.TabIndex = 2;
            this.SelectEvent.Text = "Event";
            this.SelectEvent.UseVisualStyleBackColor = true;
            this.SelectEvent.CheckedChanged += new System.EventHandler(this.SelectEvent_CheckedChanged);
            // 
            // SelectSessionIndex
            // 
            this.SelectSessionIndex.AutoSize = true;
            this.SelectSessionIndex.Location = new System.Drawing.Point(18, 53);
            this.SelectSessionIndex.Name = "SelectSessionIndex";
            this.SelectSessionIndex.Size = new System.Drawing.Size(92, 17);
            this.SelectSessionIndex.TabIndex = 1;
            this.SelectSessionIndex.Text = "Session Index";
            this.SelectSessionIndex.UseVisualStyleBackColor = true;
            this.SelectSessionIndex.CheckedChanged += new System.EventHandler(this.SelectSessionIndex_CheckedChanged);
            // 
            // SelectTimestamp
            // 
            this.SelectTimestamp.AutoSize = true;
            this.SelectTimestamp.Location = new System.Drawing.Point(18, 30);
            this.SelectTimestamp.Name = "SelectTimestamp";
            this.SelectTimestamp.Size = new System.Drawing.Size(77, 17);
            this.SelectTimestamp.TabIndex = 0;
            this.SelectTimestamp.Text = "Timestamp";
            this.SelectTimestamp.UseVisualStyleBackColor = true;
            this.SelectTimestamp.CheckedChanged += new System.EventHandler(this.SelectTimestamp_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(56, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Выберите используемые поля:";
            // 
            // SelectAll
            // 
            this.SelectAll.AutoSize = true;
            this.SelectAll.Location = new System.Drawing.Point(76, 99);
            this.SelectAll.Name = "SelectAll";
            this.SelectAll.Size = new System.Drawing.Size(124, 17);
            this.SelectAll.TabIndex = 7;
            this.SelectAll.Text = "Добавить все поля";
            this.SelectAll.UseVisualStyleBackColor = true;
            this.SelectAll.CheckedChanged += new System.EventHandler(this.SelectAll_CheckedChanged);
            // 
            // OpenFile
            // 
            this.OpenFile.Location = new System.Drawing.Point(27, 345);
            this.OpenFile.Name = "OpenFile";
            this.OpenFile.Size = new System.Drawing.Size(104, 29);
            this.OpenFile.TabIndex = 8;
            this.OpenFile.Text = "Выберите файл";
            this.OpenFile.UseVisualStyleBackColor = true;
            this.OpenFile.Click += new System.EventHandler(this.OpenFile_Click);
            // 
            // Cancel
            // 
            this.Cancel.Location = new System.Drawing.Point(277, 345);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(84, 29);
            this.Cancel.TabIndex = 9;
            this.Cancel.Text = "Отмена";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // Import
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(387, 391);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.OpenFile);
            this.Controls.Add(this.SelectAll);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Import";
            this.Text = "Import";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox SelectUser;
        private System.Windows.Forms.CheckBox SelectDevice;
        private System.Windows.Forms.CheckBox SelectPlatform;
        private System.Windows.Forms.CheckBox SelectVersion;
        private System.Windows.Forms.CheckBox SelectDescription;
        private System.Windows.Forms.CheckBox SelectEvent;
        private System.Windows.Forms.CheckBox SelectSessionIndex;
        private System.Windows.Forms.CheckBox SelectTimestamp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox SelectAll;
        private System.Windows.Forms.Button OpenFile;
        private System.Windows.Forms.Button Cancel;
    }
}