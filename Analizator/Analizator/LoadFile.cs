﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;
using System.Windows.Forms;
using Analizator;

namespace AnalizatorLogsEventFlurry
{
     public class LoadFile
    {
        /// <summary>
        /// нужно ли поле Timestamp?
        /// </summary>
        public static bool IsNeedTimestamp = false;
        /// <summary>
        /// нужно ли поле Session Index?
        /// </summary>
        public static bool IsNeedSessionIndex = false;
        /// <summary>
        /// нужно ли поле Event?
        /// </summary>
        public static bool IsNeedEvent = false;
        /// <summary>
        /// нужно ли поле Description?
        /// </summary>
        public static bool IsNeedDescription = false;
        /// <summary>
        /// нужно ли поле Version?
        /// </summary>
        public static bool IsNeedVersion = false;
        /// <summary>
        /// нужно ли поле Platform?
        /// </summary>
        public static bool IsNeedPlatform = false;
        /// <summary>
        /// нужно ли поле Device?
        /// </summary>
        public static bool IsNeedDevice = false;
        /// <summary>
        /// нужно ли поле User ID?
        /// </summary>
        public static bool IsNeedUserID = false;
        /// <summary>
        /// Успешно ли открыт файл?
        /// </summary>
        public static bool isSuccessfulOpen =false;
        /// <summary>
        /// Список имён колонок
        /// </summary>
        public List<string> NameColumnTable;
        /// <summary>
        /// Текст для парсинга, загруженный из файла
        /// </summary>
        private string _textForParsing;
        /// <summary>
        /// Количество стоблцов в таблице
        /// </summary>
        public int CountColumn;
        /// <summary>
        /// Количество строк в таблице
        /// </summary>
        public int CountRow;
        /// <summary>
        /// Массив с результатами
        /// </summary>
        public string[,] Result;
        /// <summary>
        /// Функция открытия файла
        /// </summary>
        public void OpenFile()
        {
           // Parametr = new List<ParametrsLogs>();
            OpenFileDialog  dialog = new OpenFileDialog();
            dialog.Filter = "Text documents | *.csv";
            dialog.InitialDirectory = Environment.CurrentDirectory;
            if(dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Content.Instance.Parametrs = new List<string>();
                _textForParsing = File.ReadAllText(dialog.FileName);
                //_parser();
                Content.Instance.Clear();
                _newParser();
                isSuccessfulOpen = true;
            }
            else
            {
                isSuccessfulOpen = false;
            }            
        }
        /// <summary>
        /// Задать необходимость(активность) всех полей
        /// </summary>
        /// <param name="value">Значение необходимости(активности)</param>
        public static void SetAllIsNeed(bool value)
        {
         IsNeedTimestamp = value;
         IsNeedSessionIndex = value;
         IsNeedEvent = value;
         IsNeedDescription = value;
         IsNeedVersion = value;
         IsNeedPlatform = value;
         IsNeedDevice = value;
         IsNeedUserID = value;
       }
        /// <summary>
        /// Функция парсинга
        /// </summary>
        private void _parser()
        {
            
                //разделение исходного файла по строкам
                string[] splitRow = _textForParsing.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                //замена всех пробелов на _
                splitRow[0] = splitRow[0].Replace(' ', '_');
                //задание количество строк в таблице
                CountRow = splitRow.Length - 1;
                //массиив, разделяющий по кавычкам, для отдельние первого поля с кавычками от остальных
                string[] separatorFirstFiledFromOther;
                //массив, разделяющий по запятой все поля кроме первого
                string[] separatorField;
                //инициализация начальных результатов
                Result = new string[CountRow, 20];
                //задание шапки таблицы без значений полей поля Params
                NameColumnTable = (splitRow[0].Split(new string[] { ",", "Params" }, StringSplitOptions.RemoveEmptyEntries)).ToList<string>();
                // задать количества стоблцов без учёта полей поля Params
                CountColumn = NameColumnTable.Count;
                for (int i = 1; i < splitRow.Length; i++)
                {
                    //проверка наличия в строке скобок
                    if(splitRow[i].Contains("("))
                       {
                         splitRow[i]= searchSep(splitRow[i], ',', '-');
                       }
                    //разделение строки по кавычкам
                    separatorFirstFiledFromOther = splitRow[i].Split(new char[] { '"' }, StringSplitOptions.RemoveEmptyEntries);
                    //строка значений таблицы без первого поля 
                    string stringWithoutFirstField = String.Empty;
                    //сбор строки без первого поля
                    for (int j = 1; j < separatorFirstFiledFromOther.Length; j++)
                    {
                        stringWithoutFirstField += separatorFirstFiledFromOther[j];
                    }
                //разделение по запятой
                separatorField = stringWithoutFirstField.Split(new char[] { ',' });
                    //заполнение результатов

                    Result[i - 1, 0] = separatorFirstFiledFromOther[0];
                    for (int j = 1; j < CountColumn; j++)
                    {

                      if (separatorField[j].Contains("("))
                       {
                          separatorField[j] = searchSep(separatorField[j], '-', ',');
                       }
                    Result[i - 1, j] = separatorField[j];
                    }
                    _parserParam(i - 1, separatorField[8]);
                }
                //задать конечного количества стоблцов
                CountColumn = NameColumnTable.Count;
                _setValueFilter();
                Content.Instance.NameColumn = NameColumnTable;
           
        }
        /// <summary>
        /// Вспомогательная функция, для парсинга поля Params
        /// </summary>
        /// <param name="numberRow">Номер строки</param>
        /// <param name="partParam">Значение поля Params</param>
        private void _parserParam(int numberRow,string partParam)
        {     
            //элемент удаляемый из строки     
            char signDelete = ' ';
            //удаление всех пробелов из строки     
            string[] delSpace = partParam.Split(new char[] { signDelete }, StringSplitOptions.RemoveEmptyEntries);
            partParam = string.Empty;
            for (int i=0; i < delSpace.Length; i++)
            {
                partParam += delSpace[i];
            }
            
            //разделение подполей поля Params
            string[] separatorFieldParams = partParam.Split(new char[] {';','"','{','}'}, StringSplitOptions.RemoveEmptyEntries);
            //массив, разделяющий имена полей и значения полей
            string[] separatorNameFieldFromValue;
            //количество параметров поля Params, содержащих только ':'
            int countNull = 0;
            for( int i = 0; i < separatorFieldParams.Length; i++)
            {
                if (separatorFieldParams[i].Length > 1)
                {
                    //разделение имён полей и значений полей
                    separatorNameFieldFromValue = separatorFieldParams[i].Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                    //добавление значений в массив результатов
                    Result[numberRow, i - countNull + CountColumn] = separatorNameFieldFromValue[1];
                    //добавление имён параметров в список с шапкой таблицы
                    if (!NameColumnTable.Contains(separatorNameFieldFromValue[0]))
                    {
                        NameColumnTable.Add(separatorNameFieldFromValue[0]);
                        //заполнение листа с именами параметров поля Params
                        Content.Instance.Parametrs.Add(separatorNameFieldFromValue[0]);
                    }
                    Content.Instance.AddDictionary(separatorNameFieldFromValue[0], separatorNameFieldFromValue[1]);
                }
                else
                {
                    countNull++;
                }
            }
            
        }
        /// <summary>
        /// Функция задания допустимых значений всех полей для фильтра
        /// </summary>
        private void _setValueFilter()
        {
            for(int i=0; i<CountRow; i++)
            {
                Content.Instance.AddList(Result[i, 0], Content.Instance.ValueTimestamp);
                Content.Instance.AddList(Result[i, 1], Content.Instance.ValueSessionIndex);
                Content.Instance.AddList(Result[i, 2], Content.Instance.ValueEvent);
                Content.Instance.AddList(Result[i, 3], Content.Instance.ValueDescription);
                Content.Instance.AddList(Result[i, 4], Content.Instance.ValueVersion);
                Content.Instance.AddList(Result[i, 5], Content.Instance.ValuePlatform);
                Content.Instance.AddList(Result[i, 6], Content.Instance.ValueDevice);
                Content.Instance.AddList(Result[i, 7], Content.Instance.ValueUserID);
            }
            
        }
        /// <summary>
        /// Замена символа signNeedRepace на символ signInsteadReplace, если он находится меду ( и )
        /// </summary>
        /// <param name="str">Исходная строка</param>
        /// <param name="signNeedRepace">Символ, который нужно заменить</param>
        /// <param name="signInsteadReplace">Символ, на который нужно заменить</param>
        /// <returns>Строка, в которой между ( и ) символ signNeedRepace заменён на signInsteadReplace </returns>
        private string searchSep(string str,char signNeedRepace, char signInsteadReplace)
        {
            //длина подстроки, заключенной между ( и )
            int length = 0;
            //перменная проверящаю, нужно начинать считать длину строки, заключенной между ( и )
            bool search = false;
            //индекс символа (
            int index = 0;
            //строка заключённая между ( и )
            string temp;
            for(int i=0; i < str.Length; i++)
            {
                //проверить наличие символа '('. Если он есть то, запомнить его и разрешить начать считать длину строки, заключенной между ( и )
                if (str[i] == '(')
                {
                    search = true;
                    index = i;
                }
                //проверить наличие символа ')'. Если он есть то, остановить подсчёт длины строки, заключенной между ( и )
                if (str[i] == ')')
                {
                    search = false;
                }
                //проверить, разрешено ли считать длину строки заключённой между ( и ). Если выполняется, то прибавить к длине 1
                if (search)
                {
                    length++;
                }
                //проверить остановлен ли подсчёт длины строки заключённой между ( и ) и неравна ли эта длина нулю. 
                //Если выполняется, то заменить символ signNeedRepace на signInsteadReplace
                if (!search && length != 0)
                {
                    temp = str.Substring(index, length);
                    temp =temp.Replace(signNeedRepace, signInsteadReplace);
                    str =str.Replace(str.Substring(index, length), temp);
                }

            }
            return str;
          
        }
        private void _newParser()
        {
            //разделение исходного файла по строкам
            string[] splitRow = _textForParsing.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            //замена всех пробелов на _
            splitRow[0] = splitRow[0].Replace(' ', '_');
            //задание количество строк в таблице
            CountRow = splitRow.Length - 1;
            //массив, разделяющий по кавычкам, для отдельние первого поля с кавычками от остальных
            string[] separatorFirstFiledFromOther;
            //массив, разделяющий по запятой все поля кроме первого
            string[] separatorField;
            //инициализация начальных результатов
            Result = new string[CountRow, 20];
            //задание шапки таблицы без значений полей поля Params
            NameColumnTable = (splitRow[0].Split(new string[] { ",", "Params" }, StringSplitOptions.RemoveEmptyEntries)).ToList<string>();
            // задать количества стоблцов без учёта полей поля Params
            CountColumn = NameColumnTable.Count;
            for (int i = 1; i < splitRow.Length; i++)
            {
                //проверка наличия в строке скобок
                if (splitRow[i].Contains("("))
                {
                    splitRow[i] = searchSep(splitRow[i], ',', '-','(',')');
                }
                if (splitRow[i].Contains('"'))
                {
                    splitRow[i] = searchSep(splitRow[i], ',', '-', '"', '"');
                }
                separatorField = splitRow[i].Split(new char[] { ',' });
                //заполнение результатов
                for (int j = 0; j < CountColumn; j++)
                {
                    if (separatorField[j].Contains("("))
                    {
                        separatorField[j] = searchSep(separatorField[j], '-', ',', '(', ')');
                    }
                    if (separatorField[j].Contains('"'))
                    {
                        separatorField[j] = searchSep(separatorField[j], '-', ',', '"', '"');
                    }
                    Result[i - 1, j] = separatorField[j];
                }
                _parserParam(i - 1, separatorField[8]);
            }
            //задать конечного количества стоблцов
            CountColumn = NameColumnTable.Count;
            _setValueFilter();
            Content.Instance.NameColumn = NameColumnTable;
        }
        private string searchSep(string str, char signNeedRepace, char signInsteadReplace,char leftSeparator, char rightSeparator)
        {
            //длина подстроки, заключенной между ( и )
            int length = 0;
            //перменная проверящаю, нужно начинать считать длину строки, заключенной между ( и )
            bool search = false;
            //индекс символа (
            int index = str.Length;
            //строка заключённая между ( и )
            string temp;
            //
            bool forSameSeparator = false;
            for (int i = 0; i < str.Length; i++)
            {
                //проверить наличие символа '('. Если он есть то, запомнить его и разрешить начать считать длину строки, заключенной между ( и )
                if (str[i] == leftSeparator && length==0)
                {
                    search = true;
                    index = i;
                    forSameSeparator = true;
                }
                //проверить наличие символа ')'. Если он есть то, остановить подсчёт длины строки, заключенной между ( и )
                if (str[i] == rightSeparator && !forSameSeparator)
                {
                    search = false;
                }
                //проверить, разрешено ли считать длину строки заключённой между ( и ). Если выполняется, то прибавить к длине 1
                if (search)
                {
                    length++;
                }
                //проверить остановлен ли подсчёт длины строки заключённой между ( и ) и неравна ли эта длина нулю. 
                //Если выполняется, то заменить символ signNeedRepace на signInsteadReplace
                if (!search && length != 0)
                {
                    temp = str.Substring(index, length);
                    temp = temp.Replace(signNeedRepace, signInsteadReplace);
                    str = str.Replace(str.Substring(index, length), temp);
                    length = 0;
                }
                forSameSeparator = false;
            }
            return str;
        }

    }
}
