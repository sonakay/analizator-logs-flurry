﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using AnalizatorLogsEventFlurry;

namespace Analizator
{
    public partial class SettingsFilter : Form
    {

        /// <summary>
        ///  Binding Source, привязанный к таблице с загружеными из файла данными
        /// </summary>
        BindingSource _bind;        
        /// <summary>
        /// Список ComboBoxs для параметров поля Params
        /// </summary>
        List<Control> _controlsCombobox;
        /// <summary>
        /// Текущие значения всех полей фильтра
        /// </summary>
        private List<string> currentFilter;
        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public SettingsFilter()
        {
            InitializeComponent();          
        }
        /// <summary>
        /// Конструктор, принимающий Binding Source, привязанный к таблице с загружеными из файла данными 
        /// </summary>
        /// <param name="bind">Binding Source, привязанный к таблице с загружеными из файла данными</param>
        public SettingsFilter(BindingSource bind)
        {
            InitializeComponent();
            _setValueCombobox(); 
            _bind = bind;
            _createControlParametrsForFilter();
            //определение доступности кнопки "Активировать фильтр"
            ActivateFilter.Enabled = LoadFile.isSuccessfulOpen & !Content.Instance.isFilterActive;
            OffFilter.Enabled = Content.Instance.isFilterActive;
            _checkAvailableFieldsFilter();

        }
        //Активация фильтра
        private void ActivateFilter_Click(object sender, EventArgs e)
        {
            _setCurrentValueFilter();
            ActivateFilter.Enabled = false;
            OffFilter.Enabled = true;
            string query = string.Empty;
            for(int i=0; i < currentFilter.Count; i++)
            {
                if (currentFilter[i] != " ")
                {
                    if (query == string.Empty)
                    {
                        query = string.Format("[{0}] = '{1}'", Content.Instance.NameColumn[i], currentFilter[i]);
                    }
                    else
                    {
                        query += string.Format("AND [{0}] = '{1}'", Content.Instance.NameColumn[i], currentFilter[i]);
                    }
                }
                 
            }
            if (query != string.Empty)
            {
                _bind.Filter = query;
                Content.Instance.isFilterActive = true;
            }
            else
            {
                ActivateFilter.Enabled = true;
            }  
        }
        /// <summary>
        /// Функция заполнения ComboBox значениями из поля
        /// </summary>
        private void _setValueCombobox()
        {
            SelectValueTimestamp.DataSource = Content.Instance.ValueTimestamp;
            SelectValueSessionIndex.DataSource = Content.Instance.ValueSessionIndex;
            SelectValueEvent.DataSource = Content.Instance.ValueEvent;
            SelectValueDescription.DataSource = Content.Instance.ValueDescription;
            SelectValueVersion.DataSource = Content.Instance.ValueVersion;
            SelectValuePlatform.DataSource = Content.Instance.ValuePlatform;
            SelectValueDevice.DataSource = Content.Instance.ValueDevice;
            SelectValueUserID.DataSource = Content.Instance.ValueUserID;
        }
        //Закрытие окна посредством кнопки "Отмена"
        private void Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SettingsFilter_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Задать текущие значения фильтра
        /// </summary>
        private void _setCurrentValueFilter()
        {
            currentFilter = new List<string>();
            currentFilter.Add(SelectValueTimestamp.Text);
            currentFilter.Add(SelectValueSessionIndex.Text);
            currentFilter.Add(SelectValueEvent.Text);
            currentFilter.Add(SelectValueDescription.Text);
            currentFilter.Add(SelectValueVersion.Text);
            currentFilter.Add(SelectValuePlatform.Text);
            currentFilter.Add(SelectValueDevice.Text);
            currentFilter.Add(SelectValueUserID.Text);
            foreach(var item in _controlsCombobox)
            {
                currentFilter.Add(item.Text);
            }
        }
        /// <summary>
        /// Создание элементов управления для параметров поля Params
        /// </summary>
        private void _createControlParametrsForFilter()
        {
            _controlsCombobox = new List<Control>();
            //Подпись к полям
            Label label;
            //Поле выбора значения поля
            ComboBox combobox;
            //Позиция элемнтов по у
            int pozY = 20;
            //Позиция подписи к полям по х
            int pozXlabel = 10;
            //Позиция 
            int pozXcombobox = 10;
            //Высота поля выбора значения
            int height =0;
            //Текущее количество значений
            int count =0;
            //отступ от предыдущих элементов по у
            int indent = 5;
            foreach ( var item in Content.Instance.Parametrs)
            {
                //Создание подписи к полям
                label = new Label();
                label.Name = "label" + item;
                label.Text = item + ":";
                label.Location = new Point(pozXlabel, pozY+height*count);
                //Создание поля выбора значения       
                combobox = new ComboBox();
                combobox.Name = "SelectValue" + item;
                combobox.DataSource =Content.Instance.ValueParametr[item];
                //pozXcombobox + label.Width - поставить справка от подписи к полю
                //pozY + height * count - расположение элементов ниже предыдущих на pozY
                combobox.Location = new Point(pozXcombobox + label.Width, pozY + height * count);
                count++;
                height = combobox.Height;
                pozY += indent;
                //Добавление элемнтов на панель
                ContainerForParametrsFieldParams.Controls.Add(label);
                ContainerForParametrsFieldParams.Controls.Add(combobox);
                _controlsCombobox.Add(combobox);            
            }
        }

        private void OffFilter_Click(object sender, EventArgs e)
        {
            _bind.RemoveFilter();
            OffFilter.Enabled = false;
            ActivateFilter.Enabled = true;
            Content.Instance.isFilterActive = false;
        }
        /// <summary>
        /// Проверка доступности полей фильтра
        /// </summary>
        private void _checkAvailableFieldsFilter()
        {
           SelectValueTimestamp.Enabled = LoadFile.IsNeedTimestamp;
           SelectValueSessionIndex.Enabled = LoadFile.IsNeedSessionIndex;
           SelectValueEvent.Enabled = LoadFile.IsNeedEvent;
           SelectValueDescription.Enabled = LoadFile.IsNeedDescription;
           SelectValueVersion.Enabled = LoadFile.IsNeedVersion;
           SelectValuePlatform.Enabled = LoadFile.IsNeedPlatform;
           SelectValueDevice.Enabled = LoadFile.IsNeedDevice;
           SelectValueUserID.Enabled = LoadFile.IsNeedUserID;
        }
    }
}
