﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Analizator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }
        //открытие окна импорта файла
        private void ImportMenu_Click(object sender, EventArgs e)
        {
            new Import(TableData, bindSourceTableData).ShowDialog();
        }
        //открытие окна "Настройки фильтра"
        private void MenuSettingsFilter_Click(object sender, EventArgs e)
        {
            new SettingsFilter(bindSourceTableData).ShowDialog();
        }
        //открытие окна "Статистика"
        private void StatisticMenu_Click(object sender, EventArgs e)
        {
            new Statistic(TableData).Show();          
        }       
    }
}
